//
//  ThirdTabController.swift
//  megaApp
//
//  Created by Użytkownik Gość on 29.01.2018.
//  Copyright © 2018 Użytkownik Gość. All rights reserved.
//

import Foundation
import UIKit

class ThirdTabController: UIViewController, UITableViewDelegate, UITableViewDataSource{
    
    let dbManager = DBMAnager();
    var sensors: [(String,String)] = [];
    

    override func viewDidLoad() {
        super.viewDidLoad()
        print("view did load");
        // Do any additional setup after loading the view, typically from a nib.
    }

    @IBOutlet weak var TableOutlet: UITableView!
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated);
        sensors = dbManager.getSensors();
        self.TableOutlet.reloadData();
        print(sensors);
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell2"/*Identifier*/, for: indexPath)
        cell.textLabel?.text = sensors[indexPath.row].0
        cell.detailTextLabel?.text = sensors[indexPath.row].1;
        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return sensors.count;
    }


}
