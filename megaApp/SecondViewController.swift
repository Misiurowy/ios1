//
//  SecondViewController.swift
//  megaApp
//
//  Created by Użytkownik Gość on 01.12.2017.
//  Copyright © 2017 Użytkownik Gość. All rights reserved.
//

import UIKit

class SecondViewController: UIViewController, UITableViewDelegate, UITableViewDataSource{
    let dbManager = DBMAnager();
    var readings: [(String,String)] = [];
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        print("view did appear");
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print("view did load");
        // Do any additional setup after loading the view, typically from a nib.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated);
        print("view will appear");
        readings = dbManager.gerReadings();
        self.TableViewOutlet.reloadData();
        print("view will appear");
        print(readings);
    }
    @IBOutlet weak var TableViewOutlet: UITableView!
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell"/*Identifier*/, for: indexPath)
        cell.textLabel?.text = readings[indexPath.row].0
        cell.detailTextLabel?.text = readings[indexPath.row].1;
        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return readings.count;
    }
}

